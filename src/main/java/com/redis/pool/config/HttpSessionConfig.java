package com.redis.pool.config;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.ConfigureRedisAction;

import redis.clients.jedis.JedisPoolConfig;

//@Configuration  
//@EnableRedisHttpSession  
public class HttpSessionConfig {  
	
    @Bean  
    public JedisConnectionFactory jedisConnectionFactory() {  
        String[] jedisClusterNodes = "192.168.x.x:7000,192.168.x.x:7001,192.168.x.x:7002,192.168.x.x:7003,192.168.x.x:7004,192.168.x.x:7005".split(",");  
        RedisClusterConfiguration clusterConfig=new RedisClusterConfiguration(Arrays.asList(jedisClusterNodes));  
        clusterConfig.setMaxRedirects(10);  
  
        JedisPoolConfig poolConfig=new JedisPoolConfig();  
        poolConfig.setMaxWaitMillis(1800);  
        poolConfig.setMaxTotal(2048);  
        poolConfig.setMinIdle(10);  
        poolConfig.setMaxIdle(20); 
  
        return new JedisConnectionFactory(clusterConfig, poolConfig);  
    }  
    
    
    /**
     * 让Spring Session不再执行config命令
     * @return
     */
    @Bean
    public static ConfigureRedisAction configureRedisAction() {
        return ConfigureRedisAction.NO_OP;
    }
  
}  
